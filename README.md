`unifdef`: expand macros, reduce `#ifdef`s
==========================================

_warning: unfinished nonfunctional work in progress_

The `unifdef` utility is a partial C and C++ preprocessor.
It can expand just the macros you want expanded,
selectively process `#ifdef` and `#if` conditional directves,
and simplify `#if` controlling expressions.
It does _not_ process `#include` directives.
It preserves whitespace and comments.

> Every ifdef is an ugly intrusion and a pain to read.
>
> — _Doug McIlroy_

You can use `unifdef` to manipulate code that uses `#ifdef` heavily
for portability: my original motivation was to understand `xterm`'s
`pty` handling code, one of the least portable areas of unix.

You can use `unifdef` as a lightweight preprocessor; for example [the
Linux kernel uses an older version of `unifdef`][headers_install] to
strip out `#ifdef __KERNEL__` sections from the headers it exports to
userland.

[headers_install]: http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/scripts/headers_install.sh


Documentation
-------------

  * [the `unifdef`(1) man page](doc/unifdef.html) describes:

      - the command-line usage

      - details of how `unifdef` preprocesses your code

	  - notes on standards conformance

  * This is version 3 of `unifdef`. There is a summary of
	[differences between unifdef3 and unifdef2](doc/v2v3.md).

  * [Links to reference documents written by others](doc/links.md)


Download
--------

  * Browse the source repository at:
    <https://dotat.at/cgi/git/unifdef3.git>

  * Clone the development repository with:

        git clone https://dotat.at/git/unifdef3.git


Install
-------

Release tar and zip files contain the complete source in `unifdef.c`
which is mostly standard C, except that it uses `getopt()` from
`<unistd.h>`. There is no build-time configuration, so you can usually
compile it by just running:

        make

To run the tests you will need `perl` and `awk`. We use perl's `prove`
test runner.

        make check

If you are building from the git repository you need a few external
dependencies. You can build and install them in an `ext` subdirectory
with:

        make deps

This will download, verify checksums, and buid:

  * [`re2c`](https://re2c.org/) -
    The regex-based lexical analyser in the source `unifdef.re`
	is compiled to a big DFA in `unifdef.c` by `re2c`.

  * [`mandoc`](https://mandoc.bsd.lv/) -
    Turns the `unifdef(1)` man page into nice HTML.

  * [`markdown`](https://daringfireball.net/projects/markdown/) -
	Other documentation is formatted with markdown.

I assume your OS has `perl`, for `markdown` and the build tooling in
the `scripts` subdirectory, as well as the tests.


Contribute
----------

Please send bug reports, suggestions, and patches by email to me,
Tony Finch <<dot@dotat.at>>. Any contribution that you want included
in `unifdef` must be licensed under [0BSD][] and/or [MIT-0][], and
must include a `Signed-off-by:` line to certify that you wrote it or
otherwise have the right to pass it on as a open-source patch,
according to the [Developer's Certificate of Origin 1.1][dco].

[0BSD]: https://opensource.org/license/0BSD
[MIT-0]: https://opensource.org/license/mit-0
[dco]: <https://developercertificate.org>


Licence
-------

> Written by Tony Finch <dot@dotat.at> in Cambridge.
>
> Permission is hereby granted to use, copy, modify, and/or
> distribute this software for any purpose with or without fee.
>
> This software is provided 'as is', without warranty of any kind.
> In no event shall the authors be liable for any damages arising
> from the use of this software.
>
> SPDX-License-Identifier: 0BSD OR MIT-0
