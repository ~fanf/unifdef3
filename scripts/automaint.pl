#!/usr/bin/perl
#             _  __    _      __
#   _  _ _ _ (_)/ _|__| |___ / _|  unifdef
#  | || | ' \| |  _/ _` / -_)  _|  expand macros
#   \_,_|_||_|_|_| \__,_\___|_|    reduce #ifdefs
#
# Written by Tony Finch <dot@dotat.at>
# SPDX-License-Identifier: 0BSD OR MIT-0

use warnings;
use strict;

use File::Slurp;

sub usage {
	die "usage: automaint.pl srcfile typefile funfile\n";
}

usage unless @ARGV == 3;

my ($srcfile,$typefile,$funfile) = @ARGV;

sub write_diff_file {
	my $fn = shift;
	return write_file $fn, @_
	    unless -f $fn;
	write_file "$fn.new", @_;
	my $x = system qw(diff -Tu), $fn, "$fn.new";
	if($x == 0) {
		# unchanged
		unlink "$fn.new";
	} else {
		rename $fn => "$fn.old";
		rename "$fn.new" => $fn;
	}
}

my @file = read_file $srcfile;

# Gather list of function definitions.

my %fun;

for my $i (1 .. $#file) {
	if ($file[$i-1] =~ m{^(extern|static) } and
	    $file[$i] =~ m{^(\w+)[(].*[)]\s+\{\s*.*\s*$}) {
		my $fun = $1;
		$fun{$fun} = "$file[$i-1]$file[$i]";
		$fun{$fun} =~ s{\*\n}{*};
		$fun{$fun} =~ s{\n}{ };
		$fun{$fun} =~ s{ \{\s*.*\s*$}{;}g;
	}
}

# Gather list of type definitions.

my %type;

for my $line (@file) {
	if ($line =~ m{^(typedef\s+(struct|union)\s+(\w+))\s+\{\s*$}) {
		$type{$3} = "$1 $3;";
	}
	if ($line =~ m{^(typedef\s+.*\W([A-Z]\w+);)\s*$}) {
		$type{$2} = $1;
	}
}

# Gather list of flexible array types

my %flexible;
my $flex;

for my $line (@file) {
	if ($line =~ m{^\s+Flexible;s*$}) {
		if (defined $flex) {
			$flexible{$flex} = $flex;
			undef $flex;
		} else {
			warn "unexpectedly Flexible\n";
		}
	}
	if ($line =~ m{^typedef\s+struct\s+(\w+)\s+\{\s*$}) {
		$flex = $1;
	} else {
		undef $flex;
	}
}

# Gather a list of show functions defined by SHOWER() macro calls.

my %show;

for my $line (@file) {
	$show{$1} = $1 if $line =~ m{^SHOWER[(](\w+),\s+};
}

# Gather a list of directive handlers defined by HASH() macro calls.

my %hash_fn;

for my $line (@file) {
	$hash_fn{$1} = $1 if $line =~ m{^HASH[(](\w+)[)]\s*\{\s*$};
}

# Gather a list of token types, maybe with operator precedence, from
# lexed() calls in the lexer. Used for cross-checking and sometimes
# fixing the dispatch table.

my %tok;
my %binop;
my %prec;
my %sort;
my %dict;

my $re_lexed = qr{[{]\s+lexed[(](\w+)[)];\s+[}]};

for my $line (@file) {
	# all tokens
	$tok{$1} = $1   if $line =~ m{\s+$re_lexed(?:\s+//.*)?\s*$};
	# any number -> sorting order
	$sort{$1} = $2  if $line =~ m{\s+$re_lexed\s+//\s+[+-]?(\d+)\s*$};
	# any positive number -> precedence
	$prec{$1} = $2  if $line =~ m{\s+$re_lexed\s+//\s+[+]?(\d+)\s*$};
	# explicit positive number -> standard binary operator
	$binop{$2} = $1 if $line =~ m{^'([^']+)'\s+$re_lexed\s+//\s+[+]\d+\s*$};
	# literals for the fuzzing dictionary
	$dict{$2} = $1  if $line =~ m{^["']([^"']+)['"]\s+$re_lexed};
}

# Extract the dispatch table, which we will canonicalize.

my %tbl;

my $re_tbl = qr{^\s*TOKEN\(\s*
	(\d+),\s*(\w+),\s*(\w+),\s*(\w+),\s*(\w+),\s*(\w+),\s*(\w+)
	\s*\),\s*//\s*(\w+)\s*$}x;

my @tbl_field = qw(lbp nud led tub bin hash show tok);
my %tbl_empty;
@tbl_empty{@tbl_field} = qw{ 0 error NULL NULL NULL non NULL ????? };

my @tbl_len;
$tbl_len[$_] = 0 for 0..$#tbl_field;

for my $line (@file) {
	my @row = $line =~ $re_tbl
	    or next;

	my %row; @row{@tbl_field} = @row;
	$tbl{$row{tok}} = \%row;

	for my $i (0 .. $#row) {
		$tbl_len[$i] = length $row[$i] if $tbl_len[$i] < length $row[$i];
	}
}

# Ensure every lexer token is in the table

for my $tok (sort keys %tok) {
	next if exists $tbl{$tok};
	warn "$tok in lexer but not table\n";
	my %row = %tbl_empty;
	$row{tok} = $tok;
	$tbl{$tok} = \%row;
}

# Mark every table entry that is not a lexer token

for my $tok (sort keys %tbl) {
	next if exists $tok{$tok};
	warn "$tok in table but not lexer\n";
	$tbl{$tok}{junk} = 1;
}

# Copy precedence and sorting order from punctuation operators to
# keyword operators.

for my $tok (keys %tok) {
	next unless $tok =~ m{^K_(\w+)$};
	$prec{$tok} = $prec{uc $1} if $prec{uc $1};
	$sort{$tok} = $sort{uc $1} if $sort{uc $1};
}

# Canonicalize precedence i.e. binding power fields in table.

for my $tok (keys %tok) {
	$tbl{$tok}{lbp} = $prec{$tok} // 0;
}

# Standard binary operators must have tub functions and other tokens
# should not. Break the build if they don't.

for my $tok (keys %tok) {
	if ($tbl{$tok}{led} eq 'bin') {
		$tbl{$tok}{tub} = '????' if $tbl{$tok}{tub} eq 'NULL';
	} else {
		$tbl{$tok}{tub} .= '?!' if $tbl{$tok}{tub} ne 'NULL';
	}
}

# Binary operators must have a left binding power, and other tokens
# should not, Break the build if they don't.

for my $tok (keys %tok) {
	if ($tbl{$tok}{lbp} == 0 && $tbl{$tok}{led} ne 'NULL') {
		$tbl{$tok}{lbp} = '??';
	}
	if ($tbl{$tok}{lbp} != 0 && $tbl{$tok}{led} eq 'NULL') {
		$tbl{$tok}{led} = '????';
	}
}

# Standard binary operators have known bin functions.

for my $tok (keys %tok) {
	if ($tbl{$tok}{led} eq 'bin') {
		my $bin = $tok;
		$bin =~ s{^K_(\w+)$}{uc $1}e;
		$tbl{$tok}{bin} = $bin;
	} else {
		$tbl{$tok}{bin} = 'NULL';
	}
}

# Ensure there are no NULL nud functions

for my $tok (keys %tok) {
	$tbl{$tok}{nud} = 'error' if $tbl{$tok}{nud} eq 'NULL';
}

# Ensure there are no NULL hash dispatch tables,
# and get a list for forward declaration

my %hash;
for my $tok (keys %tok) {
	my $hash = $tbl{$tok}{hash};
	if ($hash eq 'NULL') {
		$tbl{$tok}{hash} = 'non';
	} else {
		$hash{$hash} = $hash;
	}
}

# Canonicalize the character token dispatch table entries.
# Get a list of character nud function definitions.

my @ch_nud;

for my $tok (sort keys %tok) {
	next unless $tok =~ m{^CH_};
	die "weird character type $tok\n"
	    unless $tok =~ m{^CH_(\w+)_([sux])Z?$};
	my $syntax = $1;
	my $sign = $2;
	my $nud = $tok =~ s{Z$}{}r;

	my %row = %tbl_empty;
	$row{tok} = $tok;
	$row{nud} = $nud;
	$row{show} = 'character';
	$tbl{$tok} = \%row;

	my $sp = " " x (5 - length $syntax);
	push @ch_nud, <<NUD;
static NUD($nud)$sp { return(char_$sign(e, char_$syntax(e))); }
NUD
}

# uniquify
my %ch_nud;
@ch_nud{@ch_nud} = ();
@ch_nud = sort keys %ch_nud;

# Canonicalize the integer token dispatch table entries.
# Get a list of integer nud function definitions.

sub sortkey_int {
	my $tok = shift;
	$tok =~ s{^INT.(\d+[suSU])}{INT$1};
	$tok =~ s{^INT(\d[suSU])}{INT0$1};
	$tok =~ s{^ODIGITS}{0DIGITS};
	return $tok;
}
sub by_int {
	return sortkey_int($a) cmp sortkey_int($b);
}

my @int_nud;

for my $tok (sort by_int keys %tok) {
	next unless $tok =~ m{^INT};
	die "weird integer type $tok\n"
	    unless $tok =~ m{^INT(\w)(\d+)([suSU])$};
	my $prefix = $1 eq '_' ? 0 : 2;
	my $base = $2;
	my $sign = $3 eq 's' ? '  Signed' : 'Unsigned';

	my %row = %tbl_empty;
	$row{tok} = $tok;
	$row{nud} = $tok =~ s{(.)$}{lc $1}er;
	$row{hash} = $tbl{$tok}{hash};
	$row{show} = 'integer';
	$tbl{$tok} = \%row;

	my $sp = " " x (7 - length $tok);
	push @int_nud, <<NUD if $row{nud} eq $tok;
static NUD($tok)$sp { return(integer(e, $prefix, $sp$base, $sign)); }
NUD
}

# Canonicalize every show field in the dispatch table.

for my $tok (keys %tbl) {
	my $show = "operator";
	$show = "STRING" if $tok =~ m{STRING$};
	$show = "character" if $tok =~ m{^CH};
	$show = "integer" if $tok =~ m{^O?DIGITS|^INT};
	$show = "keyword" if $tok =~ m{^K};
	$show = $tok if exists $show{$tok};
	$tbl{$tok}{show} = $show;
	$show{$show} = "";
}

# Check show functions for consistency

for my $show (keys %show) {
	warn "unused show function $show\n"
	    if $show{$show} ne "";
}

# Sort tokens in a plausible order

sub sortkey {
	my $tok = shift;
	my $show = $tbl{$tok}{show};
	$show = 'keyword' if $tok =~ m{^K_};
	my $prec = $sort{$tok} // 0;
	$prec = 99 - $prec;
	$tok = sortkey_int $tok;
	return "$show $prec $tok";
}
sub by_token {
	return sortkey($a) cmp sortkey($b);
}

my @tok = sort by_token keys %tbl;

# List of functions from the table for forward declaration

my %tbl_fun;

for my $fun (qw( nud led show )) {
	for my $tok (@tok) {
		$tbl_fun{$fun}{ $fun.'_'.$tbl{$tok}{$fun} } = 1
		    if $tbl{$tok}{$fun} ne 'NULL';
	}
}

for my $fun (qw( bins binu )) {
	for my $tok (@tok) {
		my $bin = $tbl{$tok}{bin};
		$tbl_fun{$fun}{ $fun.'_'.$bin } = 1
		    if $bin ne 'NULL';
	}
}

# For rewriting sections of code between marker comments

sub maintain {
	my $slogan = shift;
	my ($i,$start,$end);
	for ($i = 0; $i < $#file; $i++) {
		if ($file[$i] =~ m{^\s*// $slogan maintained }) {
			die "started $slogan twice\n"
			    if defined $start;
			$start = $i;
		} elsif (defined $start and not defined $end and
		    $file[$i] =~ m{^\s*// end of maintained section\s*$}) {
			$end = $i;
		} elsif (defined $start and not defined $end and
			     $file[$i] =~ m{^\s*//}) {
			die "unexpected comment after $slogan\n";
		}
	}
	die "missing $slogan\n" unless defined $start;
	die "unending $slogan\n" unless defined $start;
	splice @file, $start+1, $end-$start-1, '';
	return \$file[$start+1];
}

sub wrap {
	my @wrap = (shift);
	for my $thing (@_) {
		if (length($wrap[-1]) + length($thing) > 60) {
			$wrap[-1] .= ",";
			push @wrap, $thing;
		} else {
			$wrap[-1] .= ", $thing";
		}
	}
	return @wrap;
}

sub wrap_fun {
	return '' unless @_ > 1;
	return sprintf "%s %s;", shift, join "\n\t", wrap @_;
}

# Regenerate the dispatch table

my @dispatch;
for my $tok (@tok) {
	my %row = %{ $tbl{$tok} };
	my @row; @row = @row{@tbl_field};

	for my $i (0 .. @row - 3) {
		$row[$i] .= ","
		    . " " x ($tbl_len[$i] - length $row[$i]);
	}
	$row[-2] .= " " x ($tbl_len[-2] - length $row[-2])
	    . "),// ";

	my $macro = $row{junk} ? '/////(' : 'TOKEN(';
	push @dispatch, join '', $macro, @row, "\n";
#	    " [[ ", $sort{$tok}//0, " // ", sortkey($tok), " ]]\n";
}

# Add line breaks to try to make the table easier to read

for my $i (0..$#tok) {
	$dispatch[$i] .= "\n" if $i % 10 == 9;
}

# Do the rewrites

my $dispatch = maintain 'dispatch table';
$$dispatch = join '', @dispatch;

my $ch_nud = maintain "character constant NUD functions";
$$ch_nud = join '', @ch_nud;

my $int_nud = maintain "integer constant NUD functions";
$$int_nud = join '', @int_nud;

my $binary_ops = maintain "binary operator implementations";
my @binary_ops;
for my $tok (@tok) {
	next unless exists $binop{$tok};
	my $sp1 = " " x (6 - length $tok);
	my $sp2 = " " x (2 - length $binop{$tok});
	push @binary_ops, <<BIN;
static BINS($tok)$sp1 { return(left $binop{$tok}$sp2 right); }
static BINU($tok)$sp1 { return(left $binop{$tok}$sp2 right); }
BIN
}
$$binary_ops = join '', @binary_ops;

my $flexible = maintain "union of flexible arrays";
$$flexible = join '',
    map { sprintf "\t%s\t%s;\n", $_, lc $_ }
    sort keys %flexible;

# ensure lexical keyword strings match their token type enum constants

my $lex_kw = maintain "Keywords for directives and operators";
my @lex_kw;
for my $tok (@tok) {
	next unless $tok =~ m{^K_(\w+)$};
	my $kw = $1;
	$kw = uc "__${kw}__" if $kw =~ m{^va_\w+$};
	my $tab1 = "\t" x (5 - (3 + length $kw) / 8);
	my $tab2 = "\t" x (5 - (11 + length $tok) / 8) || " ";
	my $prec = $prec{$tok} ? " // +$prec{$tok}"
		: $sort{$tok} ? " // -$sort{$tok}" : "";
	push @lex_kw, <<LEX_KW
"$kw"$tab1	{ lexed($tok);$tab2}$prec
LEX_KW
}
$$lex_kw = join '', "\n", @lex_kw, "\n";

# forward declarations of types

my $tok_enum = join "\n\t", wrap @tok;
my $enum = <<ENUM;
typedef enum Token {
	$tok_enum,
} Token;
ENUM

my @type = @type{sort keys %type};

write_diff_file $typefile, join "\n",
    "#line 2 \"$typefile\"",
    "// forward declarations of types maintained automatically",
    "", $enum, @type, "",
    "// end of maintained section\n";

# forward declarations of functions
# (and arrays of function pointers)

my @fun = @fun{sort keys %fun};

for my $fun (qw( nud led bins binu show )) {
	push @fun, "", wrap_fun "static ${fun}_fn",
	    sort keys %{ $tbl_fun{$fun} };
}

push @fun, "", wrap_fun "static hash_fn",
	    sort keys %hash_fn;

push @fun, "", wrap_fun "static Directif",
    map "hash_$_", sort keys %hash;

write_diff_file $funfile, join "\n",
    "#line 2 \"$funfile\"",
    "// forward declarations of functions maintained automatically",
    "", @fun, "",
    "// end of maintained section\n";

# fuzzer dictionary

$dict{split} = '@'; # for separating header and source
$dict{shift} = '64';
$dict{uint_max} = sprintf "%qu", -1;
$dict{sint_max} = sprintf "%qu", -1 >> 1;
$dict{xint_max} = '0x' . 'F' x 16;
$dict{oint_max} = '01' . '7' x 21;
$dict{bint_max} = '0b' . '1' x 64;

write_diff_file "t/dict",
    map "$_=\"$dict{$_}\"\n",
    sort keys %dict;

# Update the source code

write_diff_file $srcfile, @file;

write_file '.automaint', '';

exit;
