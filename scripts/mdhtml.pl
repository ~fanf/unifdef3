#!/usr/bin/perl
# SPDX-License-Identifier: 0BSD OR MIT-0

use warnings;
use strict;

use File::Slurp;

die "usage: mdhtml.pl markdown\n"
    unless @ARGV == 1;
my ($markdown) = @ARGV;

sub markdown {
	my $src = shift;
	return '' if $markdown eq 'rm';
	my $b = qx{$markdown $src};
	die if $?;
	return $b;
}

sub slurp_file {
	return $markdown eq 'rm' ? '' : read_file @_;
}

sub splat_file {
	my ($fn, $new) = @_;
	if ($markdown eq 'rm') {
		print "unlink $fn\n";
		unlink $fn;
	} else {
		my $old = -f $fn ? read_file $fn : '';
		return if $old eq $new;
		print "write $fn\n";
		write_file $fn, $new;
	}
}

if ($markdown ne 'rm' && -d '.git') {
	my @arg = ("--format='.Dd %ad'",
		   "--date=format:'%b %e, %Y'",
		   "--max-count=1");
	my $Dd = qx{git log @arg unifdef.1};
	die if $?;
	my $man = slurp_file "unifdef.1";
	$man =~ s{\n\.Dd[^\n]+\n}{\n$Dd};
	splat_file 'unifdef.1', $man;
}

my $h = slurp_file "doc/_header.html";
my $f = slurp_file "doc/_footer.html";

my $man = slurp_file "doc/_manual.html";
splat_file "doc/unifdef.html", "$h$man$f";

for my $b_md (glob "doc/*.md") {
	my $b = markdown $b_md;
	$b =~ s{\&#(\d+);}{chr $1}eg;
	$b =~ s{\&#x(..);}{chr hex $1}eg;
	my $b_html = $b_md =~ s{\.md$}{.html}r;
	splat_file $b_html, "$h$b$f";
}

write_file "doc/.doc", "";

exit 0;
