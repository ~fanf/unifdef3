#!/usr/bin/perl
# SPDX-License-Identifier: 0BSD OR MIT-0

use warnings;
use strict;

use File::Path;

sub usage {
	die "usage: download.pl file url sha256\n";
}

usage unless @ARGV == 2 or @ARGV == 3;

my ($file,$url,$sha) = @ARGV;

mkpath "ext/src";
chdir "ext/src" or die "cd ext/src: $!\n";

exit 0 if -f $file;

my @curl = (qw(curl -Lo), $file, $url);
print "@curl\n";
die "curl failed" if system @curl;
my $check = qx{openssl dgst -sha256 $file};
print $check;
die "$file checksum mismatch\n"
    if $sha and $check !~ m{\s+$sha\s*$};

exit 0;
