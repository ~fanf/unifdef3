#!/usr/bin/perl
# SPDX-License-Identifier: 0BSD OR MIT-0

use warnings;
use strict;

use Cwd;
use File::Slurp;
use File::Path;

sub usage {
	die "usage: buildext.pl progname version tgz url sha256\n";
}

usage unless @ARGV == 5;

sub mtime {
	return (stat shift)[9];
}
sub need {
	my $want = shift;
	my $mtime = mtime $want;
	unless ($mtime) {
		print "$want missing\n";
		return 1;
	}
	for my $dep (@_) {
		next if $mtime > mtime $dep;
		print "$want older than $dep\n";
		return 1;
	}
	return 0;
}
sub cd {
	print "cd @_\n";
	die "chdir @_: $!\n" unless chdir "@_";
}
sub run {
	print "run @_\n";
	die "@_ failed\n" if system @_;
}

my $cwd = cwd;
my $ext = "$cwd/ext";
my $src = "$ext/src";

my ($prog,$ver,$tgz,$url,$sha) = @ARGV;

my $qver = quotemeta $ver;
my $rever = qr{\D$qver\D};

my $base = "$prog-$ver";
my $tar = "$base.$tgz";

my $ucbase = (ucfirst $prog)."_".$ver;
my $uczip = "$ucbase.zip";
if ($tgz eq $uczip) {
	$base = $ucbase;
	$tar = $uczip;
}

my $xbin = "$ext/bin/$prog";
my $xsrc = "$ext/src/$base";
my $tsrc = "$ext/src/$base/...";
my $xtar = "$ext/src/$tar";
my $xurl = "$url/$tar";

for my $bin (split /:/, $ENV{PATH}) {
	my $pbin = "$bin/$prog";
	next unless -x $pbin;
	if ($prog eq 're2c') {
		my $v = qx($pbin -v);
		next if $?;
		next unless $v =~ $rever;
		print $v;
	}
	print "ln -s $pbin ext/bin/$prog\n";
	mkpath "$ext/bin";
	die "symlink: $!\n" unless symlink $pbin => $xbin;
	exit 0;
}

run "$cwd/scripts/download.pl", $tar, $xurl, $sha
    unless -f $xtar;

if (need $tsrc => $xtar) {
	unlink $tsrc;
	cd $src;
	run qw(tar xf), $tar if $tgz =~ /tar/;
	run qw(unzip), $tar if $tgz =~ /zip/;
	write_file $tsrc, '';
}

if ($ENV{Interpolate}) {
	my $conf = "$xsrc/$ENV{Interpolate}";
	my @conf = read_file $conf;
	for (@conf) {
		if (m{^(\w+)(\s*=\s*).*$}
		    and exists $ENV{$1}) {
			s{}{$1$2"$ENV{$1}"};
			print "interpolate $_";
		}
	}
	write_file $conf, @conf;
}

my $Makefile = $ENV{Makefile} // 'Makefile';
if (need "$xsrc/$Makefile") {
	cd $xsrc;
	run './configure', "--prefix=$ext" if -f 'configure';
}

if (need $xbin => $tsrc) {
	cd $xsrc;
	$ENV{PREFIX} = "$ext";
	$ENV{BINDIR} = "$ext/bin";
	$ENV{INCLUDEDIR} = "$ext/include/$prog";
	$ENV{LIBDIR} = "$ext/lib/$prog";
	$ENV{MANDIR} = "$ext/share/man";
	$ENV{SBINDIR} = "$ext/sbin";
	run qw(make -e all install) if -f 'Makefile';
	symlink 'share/man' => "$ext/man" unless -l "$ext/man";
	if (-f 'Markdown.pl') {
		die "chmod 0755, Markdown.pl: $!\n"
		    unless chmod 0755, 'Markdown.pl';
		die "link $xsrc/Markdown.pl => $xbin: $!\n"
		    unless link "$xsrc/Markdown.pl" => $xbin;
	}
}

exit 0;
