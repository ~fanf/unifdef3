#!/usr/bin/perl
# SPDX-License-Identifier: 0BSD OR MIT-0

use warnings;
use strict;

use File::Slurp;
use POSIX;

die "usage: upversion.pl srcfile verfile\n"
    unless @ARGV == 2;
my ($srcfile, $verfile) = @ARGV;
die "$srcfile: $!\n" unless -f $srcfile;

if (! -d '.git') {
	write_file $verfile, read_file $verfile;
	exit 0;
}

my $pv = $0 =~ s{upversion}{version}r;
my $v = qx{$pv};
chomp $v;

my $d;
if ($v =~ m{[.]XX(-|$)}) {
	my $mtime = (stat $srcfile)[9];
	$d = strftime '%Y-%m-%d %H:%M:%S %z', localtime $mtime;
} else {
	$d = qx{git show --pretty=format:%ai -s HEAD};
	die "git failed\n" if $?;
}

my $newver = <<VERSION;
	"@(#) \$Version: $v \$\\n"
	"@(#) \$Date: $d \$\\n"
VERSION

my $oldver = -f $verfile ? read_file $verfile : '';

if ($newver ne $oldver) {
	print $newver;
	write_file $verfile, $newver;
}

exit 0;
