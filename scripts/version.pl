#!/usr/bin/perl
# SPDX-License-Identifier: 0BSD OR MIT-0

use warnings;
use strict;

my $v = qx{git describe --dirty=.XX};
die "git failed\n" if $?;
$v =~ s{(\d)-g?}{$1.}g;
$v =~ s{$}{-pre-alpha};
print $v;
exit 0;
