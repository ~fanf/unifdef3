#!/bin/sh -e
# SPDX-License-Identifier: 0BSD OR MIT-0

git push chiark

# for https clone
ssh chiark git -C public-git/unifdef3.git update-server-info

# gitweb does not pull the readme from the repo
rsync -ia doc/gitweb.html chiark:public-git/unifdef3.git/README.html

# home page
rsync -ia --delete-excluded --exclude '*~' \
      ./doc/ chiark:public-html/prog/unifdef3/

exit
