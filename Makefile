#             _  __    _      __
#   _  _ _ _ (_)/ _|__| |___ / _|  unifdef
#  | || | ' \| |  _/ _` / -_)  _|  expand macros
#   \_,_|_||_|_|_| \__,_\___|_|    reduce #ifdefs
#
# Written by Tony Finch <dot@dotat.at>
# SPDX-License-Identifier: 0BSD OR MIT-0

# use these settings to build the dependencies here,
# or change them to find the dependencies elsewhere
re2c		= ext/bin/re2c
mandoc		= ext/bin/mandoc
markdown	= ext/bin/markdown

# options that might be useful (none of these are required)
# --no-debug-info		less helpful compiler errors
# --no-generation-date		reproducible builds
# -W -Werror			diagnostics
re2c_flags	= -W -Werror --no-generation-date

mandoc_flags	= -Werror -Ofragment

.PHONY: all check clean cleaner coverage docs docs-clean fuzz upload

all: deps docs unifdef

clean:
	rm -f unifdef
	rm -f t/dict t/fuzz t/fuzz-away
	rm -f *.new *.old
	rm -f *.o t/*.o
	rm -f *.gcda t/*.gcda
	rm -f *.gcno t/*.gcno
	rm -rf *.dSYM t/*.dSYM
	rm -rf t/work.*
	rm -f crash-* leak-* timeout-*
	rm -f fuzz-*.log

unifdef: unifdef.c

# machinery for generating unifdef.c

headers = forward.h functions.h pragma.h version.h

unifdef.c: ${re2c} ${headers} unifdef.re
	${re2c} ${re2c_flags} -o unifdef.c unifdef.re

version.h: scripts/upversion.pl unifdef.re
	scripts/upversion.pl unifdef.re version.h

pragma.h: pragma-dev.h pragma-dist.h
	if [ -d .git ]; \
	then ln -sf pragma-dev.h pragma.h; \
	else ln -sf pragma-dist.h pragma.h; \
	fi

forward.h functions.h: .automaint

.automaint: scripts/automaint.pl unifdef.re
	scripts/automaint.pl unifdef.re forward.h functions.h

# etags gets confused by re2c blocks so ask it to look at the
# generated C instead; re2c generates #line directives which etags
# respects, so go-to-definition will go to unifdef.re instead of
# unifdef.c

TAGS: unifdef.c
	etags unifdef.c

cleaner: clean docs-clean
	rm -f unifdef.c
	rm -f ${headers}
	rm -f .automaint
	rm -f TAGS

# tests

check: unifdef
	prove --nocolor

coverage:
	${MAKE} CFLAGS="${CFLAGS} --coverage" check

fuzz: t/fuzz t/dict t/corpus
	t/fuzz -timeout=1 -dict=t/dict t/corpus t/sources

CLANG	= clang
FUZZFLAGS= -fsanitize=fuzzer,address,signed-integer-overflow

t/fuzz: t/fuzz.c unifdef.c
	${CLANG} ${FUZZFLAGS} ${CFLAGS} ${LDFLAGS} -o t/fuzz t/fuzz.c

t/fuzz-away: t/fuzz-away.c t/fuzz.c unifdef.c
	${CC} ${CFLAGS} ${LDFLAGS} -o t/fuzz-away t/fuzz-away.c

t/dict: .automaint

t/corpus:
	mkdir -p t/corpus

# documentation

doc_md =	doc/index.md \
		doc/links.md \
		doc/v2v3.md

frag =		doc/_header.html \
		doc/_footer.html \
		doc/_manual.html

docs: doc/.doc
doc/.doc: ${markdown} ${doc_md} ${frag}
	scripts/mdhtml.pl ${markdown}

docs-clean:
	scripts/mdhtml.pl rm
	rm -f doc/_manual.html doc/index.md

doc/index.md: README.md
	sed '1,3d;s/(doc\/\([a-z0-9]*\)\.[a-z]*)/(\1.html)/' \
		<README.md >doc/index.md

doc/_manual.html: ${mandoc} unifdef.1
	${mandoc} ${mandoc_flags} -Thtml ./unifdef.1 >doc/_manual.html

# web site

upload: docs
	./scripts/publish.sh

# external dependencies

.PHONY: deps re2c mandoc markdown figlet

deps: ${re2c} ${mandoc} ${markdown}

re2c_ver	= 3.1
re2c_git	= https://github.com/skvadrik/re2c
re2c_url	= ${re2c_git}/releases/download/${re2c_ver}

re2c: ${re2c}
${re2c}:
	scripts/buildext.pl re2c ${re2c_ver} tar.xz ${re2c_url} \
		0ac299ad359e3f512b06a99397d025cfff81d3be34464ded0656f8a96676c029

mandoc: ${mandoc}
${mandoc}:
	Interpolate=configure \
	Makefile=Makefile.local \
	MANPATH_DEFAULT=$$(pwd)/ext/share/man:$$(manpath) \
	scripts/buildext.pl mandoc 1.14.6 tar.gz \
		https://mandoc.bsd.lv/snapshots/ \
		8bf0d570f01e70a6e124884088870cbed7537f36328d512909eb10cd53179d9c

markdown: ${markdown}
${markdown}:
	scripts/buildext.pl markdown 1.0.1 Markdown_1.0.1.zip \
		https://daringfireball.net/projects/downloads/ \
		6520e9b6a58c5555e381b6223d66feddee67f675ed312ec19e9cee1b92bc0137

# for use during development, not needed to build unifdef

figlet		= ext/bin/figlet

figlet: ${figlet}
${figlet}:
	DEFAULTFONTFILE=small \
	DEFAULTFONTDIR=$$(pwd)/ext/share/figlet \
	scripts/buildext.pl figlet 2.2.5 tar.gz \
		ftp://ftp.figlet.org/pub/figlet/program/unix \
		bf88c40fd0f077dab2712f54f8d39ac952e4e9f2e1882f1195be9e5e4257417d
