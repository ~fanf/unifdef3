#!/usr/bin/perl
# SPDX-License-Identifier: 0BSD OR MIT-0

use warnings;
use strict;

use Cwd;
use File::Path;
use Test::More;

my $cwd = cwd;

# test output written to files in temporary directory
my $work = "$cwd/t/work.$$";

# test case scripts and exemplar stdout and stderr
my $cases = "$cwd/t/cases";

# source files used by test case scripts: this is the
# current directory when running the tests so that the
# test case scripts can refer to source files easily
my $sources = "$cwd/t/sources";

# test scripts should run $unifdef
$ENV{unifdef} = "$cwd/unifdef";

# for test cases to access shared scripts
$ENV{here} = $cases;

mkdir $work or die "mkdir $work: $!\n";
chdir $sources or die "chdir $sources: $!\n";

sub diff {
	my ($test,$right,$wrong) = @_;
	my $diff = qx{diff -Nu $right $wrong};
	is $?, 0, "$test diff ran OK";
	is $diff , '', "$test matched";
}

for my $sh (glob "$cases/*.sh") {
	my $re = qr{.*/([\w-]+)\.(\d+)\.sh$};
	like $sh, $re, "check test name";
	next unless $sh =~ m{$re};
	my $test = $1;
	my $exit = $2;
	my $t = "$cases/$test";
	my $w = "$work/$test";
	system "sh -eu $sh 1>$w.out 2>$w.err";
	is $?, $exit, "$test exit status matches";
	diff "$test stdout", "$t.out", "$w.out";
	diff "$test stderr", "$t.err", "$w.err";
}

if (Test::More->builder->is_passing) {
	rmtree $work;
}

done_testing;
