/*             _  __    _      __
 *   _  _ _ _ (_)/ _|__| |___ / _|  libfuzzer
 *  | || | ' \| |  _/ _` / -_)  _|  test
 *   \_,_|_||_|_|_| \__,_\___|_|    harness
 *
 * Written by Tony Finch <dot@dotat.at>
 * You may do anything with this. It has no warranty.
 * <https://creativecommons.org/publicdomain/zero/1.0/>
 * SPDX-License-Identifier: CC0-1.0
 */

#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>

#define main main_is_disabled
extern int main(int argc, char *argv[]);

#include "../unifdef.c"

#undef main

/*
 * helpers
 */

static Buffer *
fuzz_lex_part(Position *here, const char *name, span input) {
	Buffer *buf = buf_cat(NULL, input);
	*here = lex_start(span_str(name), span_buf(buf));
	return(buf);
}

static Buffer *
fuzz_lex_start(Position *here, const byte *input, size_t size) {
	return(fuzz_lex_part(here, "source",
			     (span){ input, input + size }));
}

/*
 * When there is no header the output must match the input
 */
static int
fuzz_noop(const byte *input, size_t size) {
	Position there;
	Position *here = &there;
	Buffer *source = NULL;
	Output out = {0};

	source = fuzz_lex_start(here, input, size);
	preprocess(&out, here);

	assert(out.buf->used == size);
	assert(memcmp(out.buf->base, input, size) == 0);

	FREE(source);
	FREE(out.buf);
	FREE(out.stack);
	return(0);
}

/*
 * Only the first token may start with backslash-newline
 */
static int
fuzz_lex_trailing_backslash_newline(const byte *input, size_t size) {
	Position there;
	Position *here = &there;
	Buffer *source = NULL;

	source = fuzz_lex_start(here, input, size);
	for(;;) {
		Token tok = lex(here);
		if(tok == NOMORE)
			break;
		assert(backslash_newline(here->txt.ptr) == here->txt.ptr);
	}

	FREE(source);
	return(0);
}

/*
 * Break the input into a header file and a source file.
 *
 * We split on '@' because that's an uninteresting non-control character.
 * Breaking on control characters doesn't work with libfuzzer's only_ascii
 * option. And even in binary mode we don't want to split on '\0' because
 * it has special interactions with EOF inside re2c, and we want to allow
 * it in either input file.
 */
static int
fuzz_no_crashes_or_leaks(const byte *input, size_t size) {
	Position there;
	Position *here = &there;
	Buffer *header = NULL;
	Buffer *source = NULL;
	Output out = {0};

	const byte *split = memchr(input, '@', size);
	const byte *limit = input + size;
	if(split != NULL) {
		header = fuzz_lex_part(here, "header",
				       (span){ input, split });
		header_parse(&out.st, here);
		source = fuzz_lex_part(here, "source",
				       (span){ split+1, limit });
	} else {
		source = fuzz_lex_start(here, input, size);
	}

	preprocess(&out, here);

	FREE(header);
	FREE(source);
	FREE(out.buf);
	FREE(out.stack);
	symtab_free(&out.st);
	return(0);
}

/*
 * Ensure every token in the output appears in the input, unless there are
 * paste tokens in the macros. (This test does everything that
 * fuzz_no_crashes() does so they are a bit redundant...)
 */
static int
fuzz_output_tokens_in_input(const byte *input, size_t size) {
	Position there, rewind;
	Position *here = &there;
	Buffer *header = NULL;
	Buffer *source = NULL;
	Table *seen = NULL;
	Output out = {0};

	const byte *split = memchr(input, '@', size);
	const byte *limit = input + size;
	if(split == NULL)
		goto out;

	header = fuzz_lex_part(here, "header",
			       (span){ input, split });
	rewind = there;
	while(lex(here) != NOMORE)
		if(here->tok == PASTE)
			goto out;
	there = rewind;
	header_parse(&out.st, here);

	source = fuzz_lex_part(here, "source",
			       (span){ split+1, limit });
	rewind = there;
	while(lex(here) != NOMORE)
		seen = tab_add(seen, here->txt);
	there = rewind;
	preprocess(&out, here);

	there = lex_start(span_str("output"), span_buf(out.buf));
	while(lex(here) != NOMORE)
		assert(tab_find(seen, here->txt) != MISSING);

out:
	FREE(seen);
	FREE(header);
	FREE(source);
	FREE(out.buf);
	FREE(out.stack);
	symtab_free(&out.st);
	return(0);
}

static int
waitfor(pid_t pid) {
	int status, ret;
	do {
		ret = waitpid(pid, &status, 0);
	} while(ret < 0 && errno == EINTR);
	if(ret < 0)
		printf("wait: %s\n", strerror(errno));
	else if(ret != pid)
		printf("wait %d expected %d\n", ret, pid);
	return(status);
}

static int
fork_exec(const char *argv[]) {
	pid_t pid = fork();
	assert(pid >= 0);
	if(pid < 0) {
		printf("fork: %s\n", strerror(errno));
		exit(1);
	}
	if(pid > 0)
		return(waitfor(pid));
	// child
	execvp(argv[0], (void*)argv);
	printf("exec: %s\n", strerror(errno));
	exit(1);
}

/*
 * Check that we evaluate expressions the same as the compiler
 */
static int
fuzz_eval_matches_cc(const byte *input, size_t size) {
	Position there;
	Position *here = &there;
	Expression e = { here, NULL };
	Buffer *source = fuzz_lex_start(here, input, size);

	// Skip any expressions that can produce valueless results hidden
	// by simplification - not quite the same as is_symbol() because we
	// want to allow true and false (which can be #defined) but deny
	// defined (which cannot be).
	//
	// Ensure all numbers have LL suffixes, to stop the compiler
	// evaluating at the wrong width.
	//
	// Allow exactly one character constant, to avoid problems with type
	// promotions. We want to fuzz character constants (it has been very
	// good at finding bugs) but not arithmetic on character constants
	// (it is too hard to match the compiler).
	//
	// But always skip utf8 character constants and universal character
	// names because we implement them in a less crazy manner than gcc
	// and clang (though that might be a bad idea).
	//
	Position rewind = there;
	bool nonchar = false, onechar = false;
	lex(here); // skip initial artifical newline
	for(Token tok = here->tok; tok != NOMORE; tok = lex(here)) {
		if(token[tok].nud == nud_unknown ||
		   token[tok].nud == nud_novalue ||
		   token[tok].nud == nud_defined ||
		   tok == INTb2s || tok == INTb2u ||
		   tok == INT_8s || tok == INT_8u ||
		   tok == INT_10s || tok == INT_10u ||
		   tok == INTx16s || tok == INTx16u ||
		   tok == DIGITS || tok == ODIGITS ||
		   tok == CH_utf2_sZ || tok == CH_utf2_uZ ||
		   tok == CH_utf3_sZ || tok == CH_utf3_uZ ||
		   tok == CH_utf4_sZ || tok == CH_utf4_uZ ||
		   tok == CH_byte_uZ || tok == CH_byte_xZ ||
		   tok == CH_esc_uZ || tok == CH_oct_uZ ||
		   tok == CH_hex_sZ || tok == CH_hex_uZ ||
		   tok == CH_ascii_uZ)
			goto out;
		if(tok == CH_esc_s || tok == CH_esc_u ||
		   tok == CH_hex_s || tok == CH_hex_u || tok == CH_hex_x ||
		   tok == CH_oct_s || tok == CH_oct_u || tok == CH_oct_x ||
		   tok == CH_ascii_s || tok == CH_ascii_u) {
			if(nonchar) goto out;
			else onechar = true;
		} else {
			if(onechar) goto out;
			else nonchar = true;
		}
	}
	there = rewind;

	// eat initial newline, because eval() will not
	lex(here);
	Integer i = eval(&e, 0);
	if(valueless(i.type)) {
		if(dflag('F')) eprintf("valueless\n");
		goto out;
	}
	if(here->tok != NOMORE) {
		if(dflag('F')) eprintf("more\n");
		goto out;
	}
	pid_t pid = fork();
	assert(pid >= 0);
	if(pid != 0) {
		int status = waitfor(pid);
		char out[24];
		snprintf(out, sizeof(out), "t/work.%d.out", (int)pid);
		const char *cat_argv[] = { "cat", out, NULL };
		if(status != 0 && dflag('F')) {
			printf("cat %s\n", out);
			fork_exec(cat_argv);
		}
		assert(status == 0);
	out:	FREE(source);
		FREE(e.del);
		return(0);
	}

	// in the child...
	pid = getpid();
	char prog[24], prog_c[24], prog_out[24];
	snprintf(prog, sizeof(prog), "t/work.%d", (int)pid);
	snprintf(prog_c, sizeof(prog_c), "%s.c", prog);

	// redirect output
	snprintf(prog_out, sizeof(prog_out), "%s.out", prog);
	int fd = open(prog_out, O_CREAT|O_WRONLY, 0666);
	if(fd < 0 || dup2(fd, 2) < 0 || dup2(fd, 1) < 0) {
		printf("write %s: %s\n", prog_out, strerror(errno));
		exit(1);
	}

	// write a program so the compiler can evaluate our expression...
	// we need <stdbool.h> and <iso646.h> so that the C compiler
	// supports the expression features that we do, and we need to
	// break the line after the expression under test so that it
	// doesn't make a mess if there is a trailing // line comment
	FILE *fp = fopen(prog_c, "w");
	if(fp == NULL)
		goto prog_c_error;
	fprintf(fp,
		"#include <stdio.h>\n"
		"#include <stdint.h>\n"
		"#include <stdbool.h>\n"
		"#include <iso646.h>\n"
		"int main(void) {\n"
		"	printf(\"%%ju\\n\", (uintmax_t)(%.*s\n\n));\n"
		"	return(0);\n"
		"}\n",
		(int)size, (const char *)input);
	if(fclose(fp) < 0) {
	prog_c_error:
		printf("write %s: %s\n", prog_c, strerror(errno));
		exit(1);
	}

	// compile and run program
	const char *cc_argv[] = {
		"cc", "-w", "-o", prog, prog_c, NULL
	};
	if(fork_exec(cc_argv) != 0)
		exit(1);
	const char *prog_argv[] = {
		prog, NULL
	};
	if(fork_exec(prog_argv) != 0)
		exit(1);

	// examine output
	char mine32s[24], mine32u[24], mine64[24], yours[24];
	fp = fopen(prog_out, "r");
	if(fp == NULL) {
		printf("read %s: %s\n", prog_out, strerror(errno));
		exit(1);
	}
	fgets(yours, sizeof(yours), fp);
	snprintf(mine32s, sizeof(mine32s), "%ju\n", (uintmax_t)(int)i.val);
	snprintf(mine32u, sizeof(mine32u), "%u\n", (unsigned)i.val);
	snprintf(mine64, sizeof(mine64), "%ju\n", i.val);
	if(strcmp(mine32s, yours) != 0 &&
	   strcmp(mine32u, yours) != 0 &&
	   strcmp(mine64, yours) != 0 ) {
		printf("expected 32s %s", mine32s);
		printf("expected 32u %s", mine32u);
		printf("expected 64 %s", mine64);
		fflush(stdout);
		exit(1);
	}

	remove(prog);
	remove(prog_c);
	remove(prog_out);

	// if we exit normally then libfuzzer gets upset with us
	_exit(0);
}

static int
fuzz(byte mode, const byte *input, size_t size) {
	switch(mode) {
	case(0): return(fuzz_noop(input, size));
	case(1): return(fuzz_lex_trailing_backslash_newline(input, size));
	case(2): return(fuzz_no_crashes_or_leaks(input, size));
	case(3): return(fuzz_output_tokens_in_input(input, size));
	case(4): return(fuzz_eval_matches_cc(input, size));
	default: return(0);
	}
}

extern int
LLVMFuzzerTestOneInput(const byte *input, size_t size);
extern int
LLVMFuzzerTestOneInput(const byte *input, size_t size) {
	if(dflag('z'))
		dflags = set_flags(dflags, (BC)"abcdefghijklmnopqrstuvwxyz");
	if(size == 0)
		return(0);
	return(fuzz(*input, input+1, size-1));
}
