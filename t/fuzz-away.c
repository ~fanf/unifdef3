/*             _  __    _      __
 *   _  _ _ _ (_)/ _|__| |___ / _|  libfuzzer
 *  | || | ' \| |  _/ _` / -_)  _|  test
 *   \_,_|_||_|_|_| \__,_\___|_|    harness
 *
 * Written by Tony Finch <dot@dotat.at>
 * You may do anything with this. It has no warranty.
 * <https://creativecommons.org/publicdomain/zero/1.0/>
 * SPDX-License-Identifier: CC0-1.0
 */

#include "fuzz.c"

int
main(int argc, char *argv[]) {
	if(argc != 3) {
		printf("usage: fuzz-away <mode> <string>\n");
		return(1);
	}
	dflags = set_flags(dflags, (BC)"ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	return(fuzz(atoi(argv[1]), (BC)argv[2], strlen(argv[2])));
}
